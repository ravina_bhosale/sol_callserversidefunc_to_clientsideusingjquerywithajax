﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_ServerSideToClientSide__Addition
{
    public partial class Addition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public Api 
        [WebMethod]
        public static int AdditionNosApi(int val1,int val2)
        {
            return val1 + val2;

        }
        #endregion 

    }
}