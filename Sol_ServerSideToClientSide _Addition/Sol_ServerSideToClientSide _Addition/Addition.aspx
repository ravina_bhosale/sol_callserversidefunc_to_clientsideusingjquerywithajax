﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Addition.aspx.cs" Inherits="Sol_ServerSideToClientSide__Addition.Addition" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    

    <script type="text/javascript" src="Scripts/jquery-3.1.1.min.js">
    </script>


    <script type="text/javascript">

        function pageLoad() {

            $(document).ready(function () {

                //alert("Test");

                //Fire Button Click event to check login
                $("#btnCalculate").click(function () {
                    
                    // read value from TextBoxes.
                    var Value1 = parseInt($("#txtValue1").val());
                    var Value2 = parseInt($("#txtValue2").val());
                    

                    // call Asp.net Web api
                    PageMethods.AdditionNosApi(Value1, Value2,
                      function (result) {

                          // Display Message Panel with Message

                          $("#lblMessage").text(result);

                          //$("#divMessagePanel")
                          // .show()
                           //.fadeOut(5000, function () {
                           //    result
                               
                           //});
                      });
                });
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:ScriptManager ID="scriptManger" runat="server" EnablePageMethods="true">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>

                    <table>
                        <tr>
                            <td>
                                <input type="text" id="txtValue1" placeholder="EnterNumber" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" id="txtValue2" placeholder="EnterNumber" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" id="btnCalculate" value="Addition" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divMessagePanel" >
                                    <span id="lblMessage"></span>
                                </div>
                            </td>
                        </tr>


                    </table>


                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
